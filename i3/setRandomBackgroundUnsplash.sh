#!/bin/bash
wget -q --spider http://yandex.ru
if [ $? -eq 0 ]; then
	dateTime=$(date +%s)
	picDir=$(xdg-user-dir PICTURES)
	getFullScreenSize=$(xdpyinfo | awk '/dimensions:/ { print $2; exit }')
	getOneScreenSize=$(xrandr --current | sed -n 's/.* connected \([0-9]*\)x\([0-9]*\)+.*/\1x\2/p' | head -n 1)
	input=$1
	attributes=$2
	if [ "$input" == "full" ] 
	then
		wget -q  https://source.unsplash.com/random/$getFullScreenSize/?$attributes -O $picDir/$dateTime.jpg
	elif [ "$input" == "one" ]
	then
		wget -q https://source.unsplash.com/random/$getOneScreenSize/?$attributes -O $picDir/$dateTime.jpg
	else
		wget -q https://source.unsplash.com/random/$getOneScreenSize/?$attributes -O $picDir/$dateTime.jpg
	fi
	feh --bg-tile $picDir/$dateTime.jpg
	rm -f $picDir/$dateTime.jpg
else
	bash $HOME/.config/i3/setRandomBackgroundUnsplash.sh $1 $2
fi
