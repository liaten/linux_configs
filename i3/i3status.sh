#!/bin/bash
echo '
{
	"version": 1
}
[
	[]'

# Now send blocks with information forever:
while :;
do

	now_date=$(date +%a" "%d/%m/%Y" "%H:%M)

	echo ",
[

{
        \"name\":\"id_language\",
        \"full_text\":\"$lang\"
},

{
	\"name\":\"id_time\",
	\"full_text\":\"$now_date\"
}

]"
	sleep 10
done
