#!/bin/bash

matches=$(find $HOME/.config/i3/ -name dummySamsung|wc -l)

xrandr -s 0 && xrandr --output HDMI-A-2 --primary --rate 74.97

if [[ $matches -gt 0 ]]; then
	n=$(cat $HOME/.config/i3/dummySamsung)
	if [[ $n -eq 0 ]]; then
		xrandr --output DVI-D-0 --auto --right-of HDMI-A-2 && xrandr --output DisplayPort-1 --auto --left-of HDMI-A-2
		echo 1 > $HOME/.config/i3/dummySamsung	
	else
		xrandr --output DVI-D-0 --auto --right-of HDMI-A-2
		echo 0 > $HOME/.config/i3/dummySamsung
	fi
else
	touch $HOME/.config/i3/dummySamsung
	echo 0 > $HOME/.config/i3/dummySamsung
	xrandr --output DVI-D-0 --auto --right-of HDMI-A-2 && xrandr --output DisplayPort-1 --auto --left-of HDMI-A-2
fi
