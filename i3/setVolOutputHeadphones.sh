#!/bin/bash

id=$1

amixer -q -c $id set Headphone 100%
amixer -q -c $id set Front 0%
amixer -q -c $id set Headphone unmute
amixer -q -c $id set Front mute
amixer -q -c $id set Master 100%
amixer -q -c $id set Master unmute
