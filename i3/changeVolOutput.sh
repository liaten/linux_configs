#!/bin/bash

id=$1
vol=$(amixer -c $id sget Front | grep 'Left:' | awk -F'[][]' '{ print $2 }' | tr -d %)
if [ "$vol" == "0" ] ; then
	amixer -q -c $id set Headphone 0%
	amixer -q -c $id set Front 100%
elif [ "$vol" == "100" ] ; then
	amixer -q -c $id set Headphone 100%
        amixer -q -c $id set Front 0%
else
	exit
fi
amixer -q -c $id set Headphone unmute
amixer -q -c $id set Front unmute
amixer -q -c $id set Master 100%
amixer -q -c $id set Master unmute
