#!/bin/bash

sha1=$(sha512sum debian-11.4.0-amd64-netinst.iso | head -n1 | awk '{print $1;}')
sha2=$(cat SHA512SUMS | head -n1 | awk '{print $1;}')

if [[ sha1==sha2 ]]; then
	echo true
else
	echo false
fi
