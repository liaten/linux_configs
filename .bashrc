alias work='ssh atm@192.168.3.8'
alias c='clear'
alias l='ls -AFG --color=auto'
alias ll='l -l'
alias upd='apt update && apt upgrade -y && apt dist-upgrade && apt autoremove && apt clean && do-release-upgrade'
VISUAL=vi
EDITOR=$VISUAL
ANDROID_SDK_ROOT=(/usr/lib/android-sdk)

parse_git_branch() {
     git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/(\1)/'
}

#PS1='\[\033[1;36m\]\u\[\033[1;31m\]@\[\033[1;32m\]\h:\[\033[1;35m\]\w\[\033[1;31m\]\$\[\033[0m\]\$(parse_git_branch)\[e\[00m\]$'
#export PS1="\u@\h \[\e[32m\]\w \[\e[91m\]\$(parse_git_branch)\[\e[00m\]$ "
export PS1="\[\033[1;36m\]\u\[\033[1;31m\]@\[\033[1;32m\]\h:\[\033[1;35m\] \[\e[32m\]\w \[\e[91m\]\$(parse_git_branch)\[\e[00m\]$ "
force_color_prompt=yes
